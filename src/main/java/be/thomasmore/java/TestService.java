/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java;

import java.io.InputStream;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author StijnSoete
 */
@Stateless
@Path("test")
public class TestService {

    private final ExcelReader excelReader = new ExcelReader();
    @PersistenceContext(unitName = "MavenPU")
    private EntityManager em;

    public List<Score> readTestFromExcel(InputStream xslxFile) {
        List<Score> scores = excelReader.readTest(xslxFile);
        return scores;
    }

    @POST
    @Path("uploadTest")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    public String readTestFromExcelAndStore(@FormDataParam("file") InputStream xslxFile,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {
        List<Score> scores = storeTest(readTestFromExcel(xslxFile));
        if (scores == null) {
            return "<h1>Fout bij het verwerken</h1> <p>Zorg dat het bestand voldoet aan de gegeven template.</p>";
        }
        String result = "<h1>Scores</h1>";
        if (scores.isEmpty()) {
            result += "<p>Er zijn geen scores aanwezig in het bestand</p>";
        }
        for (Score score : scores) {
            result += "<p>" + score.getStudent().toString() + " behaalde " + score.getScore() + " op de " + score.getTest().toString() + "</p>";
        }
        return result;
    }

    public List<Score> storeTest(List<Score> scores) {
        if (scores == null) {
            return null;
        }
        Student student = null;
        Test test = null;
        Score oldScore;
        for (Score score : scores) {
            //If the student already exists this part makes sure it doesnt duplicate it.
            try {
                Query q = em.createNamedQuery("Student.getStudentByRNummer");
                q.setParameter("rNummer", score.getStudent().getStudentNummer());
                student = (Student) q.getSingleResult();
                score.setStudent(student);
            } catch (NoResultException e) {
                //This is fine, it just means this particular student doesn't exist in the database yet.
            }
            if (test == null) {
                //If the test already exists this part makes sure it doesnt duplicate it.
                try {
                    Query q = em.createNamedQuery("Test.getByAllParams");
                    q.setParameter("naam", score.getTest().getNaam());
                    q.setParameter("vakNaam", score.getTest().getVakNaam());
                    q.setParameter("maxScore", score.getTest().getMaxScore());
                    q.setParameter("datum", score.getTest().getDatum());
                    test = (Test) q.getSingleResult();
                    score.setTest(test);
                } catch (NoResultException e) {
                    //This is fine, it just means this particular test doesn't exist in the database yet.
                }
            } else {
                score.setTest(test);
            }

            //If the student score for this test already exists this part makes sure it updates the old score to the new one.
            try {
                Query q = em.createNamedQuery("Score.getScoreByStudentAndTest");
                q.setParameter("student", student);
                q.setParameter("test", test);
                oldScore = (Score) q.getSingleResult();
                oldScore.setScore(score.getScore());
                em.persist(oldScore);
            } catch (NoResultException e) {
                //This is fine, it just means this particular score will be freshly added to the database.
                em.persist(score);
            }
        }
        return scores;
    }
    public List<Test> findAll() {
        Query q = em.createNamedQuery("Test.getAll");
        return q.getResultList();
    }

}
