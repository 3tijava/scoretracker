/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author StijnSoete
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "Score.getScoreByStudentAndTest", query = "SELECT s FROM Score s WHERE s.student = :student AND s.test = :test"),
        @NamedQuery(name= "Score.getScoresByTest", query= "select s from Score s where s.test.id = :testId order by s.score"),
        @NamedQuery(name = "Score.getAvgSingleStudent", query = "SELECT (SUM(s.score) * 100 / SUM(s.test.maxScore)), s.test.vakNaam FROM Score s WHERE s.student = :student AND :beginDatum < s.test.datum AND s.test.datum < :eindDatum GROUP BY s.test.vakNaam"),
        @NamedQuery(name = "Score.getAvgClassGroupedByCourse",
                query = "SELECT (SUM(s.score) * 100 / SUM(s.test.maxScore)), s.student.studentNummer FROM Score s WHERE s.test.vakNaam = :vak AND s.student.klas = :klas AND :beginDatum < s.test.datum AND s.test.datum < :eindDatum GROUP BY s.student.studentNummer ORDER BY SUM(s.score)")

})
public class Score implements Serializable {
    private int score;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Student student;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Test test;
          


    public Score(int score) {
        this.score = score;
    }

    public Score() {
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    
    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Score)) {
            return false;
        }
        Score other = (Score) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "be.thomasmore.java.Score[ id=" + id + " ]";
    }

}
