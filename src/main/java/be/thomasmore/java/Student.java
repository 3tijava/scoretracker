/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author StijnSoete
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "Student.getStudentByRNummer", query = "SELECT s FROM Student s WHERE s.studentNummer = :rNummer"),
	@NamedQuery(name = "Student.getAllStudenten", query = "SELECT s FROM Student s")
})
@XmlRootElement
public class Student implements Serializable {

    private String firstName, lastName, email, klas, studentNummer;
    @OneToMany (mappedBy="student")
    public List<Score> scores = new ArrayList<>();

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Student(String firstName, String lastName, String email, String klas, String studentNummer, Long id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.klas = klas;
        this.studentNummer = studentNummer;
        this.id = id;
    }

    public Student() {
    }

    public String getKlas() {
        return klas;
    }

    public void setKlas(String klas) {
        this.klas = klas;
    }

    @XmlTransient
    public List<Score> getScores() {
        return scores;
    }

    public String getStudentNummer() {
        return studentNummer;
    }

    public void setStudentNummer(String studentNummer) {
        this.studentNummer = studentNummer;
    }
    

    public void setScores(List<Score> scores) {
        this.scores = scores;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Student)) {
            return false;
        }

        Student student = (Student) o;

        if (getLastName() != null && !getLastName().equals(student.getLastName())) {
            return false;
        }
        if (getFirstName() != null && !getFirstName().equals(student.getFirstName())) {
            return false;
        }
        return getEmail() != null && getEmail().equals(student.getEmail());

    }

    public Student(String firstName, String lastName, String email, String klas, String studentNummer) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.klas = klas;
        this.studentNummer = studentNummer;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        if(klas != null)
            return studentNummer + ": " + firstName + " " + lastName + " (" + klas + ")";
        return studentNummer + ": " + firstName + " " + lastName + " (Niet in een klas ingedeeld)";
    }

}
