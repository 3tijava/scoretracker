package be.thomasmore.java;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author r0591954
 */

@Stateless
@Path("studentreport")
public class PeriodicReportService {
    @PersistenceContext(unitName = "MavenPU")
    private EntityManager em;
    
    @GET
    @Path("getpdf/{rNummer}/{beginDatum}/{eindDatum}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response createPeriodicReportStudent(@PathParam("rNummer") String rNummer, @PathParam("beginDatum") String sbeginDatum, @PathParam("eindDatum") String seindDatum){
        Date beginDatum = null, eindDatum = null;
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            beginDatum = sdf.parse(sbeginDatum);
            eindDatum = sdf.parse(seindDatum);
        } catch (ParseException ex) {
            Logger.getLogger(PeriodicReportService.class.getName()).log(Level.SEVERE, null, ex);
        }
        File temp = createPDFStudent(rNummer, beginDatum, eindDatum);
        Response.ResponseBuilder response = Response.ok((Object) temp);
        response.header("Content-Disposition", "attachment; filename=testRapport.pdf");
        return response.build();
    }
    
    public Student getStudentByRNummer(String rNummer) {
        Query q = em.createNamedQuery("Student.getStudentByRNummer");
        q.setParameter("rNummer", rNummer);
        return (Student)q.getSingleResult();
    }
    
    public List<Object[]> getAvgSingleStudentGroupedByCourseFilteredByDate(Student student, Date beginDatum, Date eindDatum) {
        Query q = em.createNamedQuery("Score.getAvgSingleStudent");
            q.setParameter("student", student);
            q.setParameter("beginDatum", beginDatum);
            q.setParameter("eindDatum", eindDatum);
        return (List<Object[]>)q.getResultList();
    }
    
    public File createPDFStudent(String rNummer, Date beginDatum, Date eindDatum) {
        try {
            Student student;
            List<Object[]> results;
            int[] gemiddeldeEnMediaanKlas;
            int gemiddelde;
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            student = getStudentByRNummer(rNummer);
            results = getAvgSingleStudentGroupedByCourseFilteredByDate(student, beginDatum, eindDatum);
            
            Document document = new Document();
            // Tijdelijk bestand aanmaken (PDF)
            File temp = File.createTempFile("resultaat" + rNummer, ".pdf");
            //PDF openen en bewerken
            PdfWriter.getInstance(document, new FileOutputStream(temp.getAbsolutePath()));
            document.open();
            // MetaData toevoegen
            document.addTitle("Resulaat_ " + rNummer);
            document.addAuthor("ScoreTracker");
            document.addCreator("ScoreTracker");
            Font titleFont = new Font(FontFactory.getFont(FontFactory.TIMES, 18f));
            titleFont.setColor(33, 84, 172);

            Font subtitleFont = new Font(FontFactory.getFont(FontFactory.TIMES, 16f));
            subtitleFont.setColor(29, 100, 150);

            Font boldFont = new Font(FontFactory.getFont(FontFactory.TIMES_BOLD, 14f));
            boldFont.setColor(0, 0, 0);

            // Titel toevoegen
            Paragraph preface = new Paragraph();
            Paragraph preface1 = new Paragraph();
            preface.add(new Paragraph("Rapport voor student " + student.toString(), titleFont));
            preface1.add(new Paragraph("Periode:" + dateFormat.format(beginDatum) + " - " + dateFormat.format(eindDatum), titleFont));
            document.add(preface);
            document.add(preface1);
            // Tabel toevoegen met 5 kolommen
            PdfPTable table = new PdfPTable(4);
            PdfPCell c1 = new PdfPCell(new Phrase("Vak", subtitleFont));
            c1.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(c1);
            PdfPCell c2 = new PdfPCell(new Phrase("Resultaat", subtitleFont));
            c2.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(c2);
            PdfPCell c3 = new PdfPCell(new Phrase("Klas gemiddelde", subtitleFont));
            c3.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(c3);
            PdfPCell c4 = new PdfPCell(new Phrase("Klas mediaan", subtitleFont));
            c4.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(c4);
            table.setHeaderRows(1);

            //tabel opvullen met scores uit "List<Score> scores"
            
            for (Object[] result : results){
                gemiddelde = (int)(long)result[0];
                gemiddeldeEnMediaanKlas = getAverageAndMedianKlasVakScore((String) result[1], student.getKlas(), beginDatum, eindDatum);
                
                table.addCell((String) result[1]);
                table.addCell(gemiddelde + "");
                table.addCell(gemiddeldeEnMediaanKlas[0] +"");
                table.addCell(gemiddeldeEnMediaanKlas[1] +"");
            }
            document.add(table);

            document.close();
            return temp;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
    public List<Object[]> getAvgClassGroupedByCourse(String vak, String klas, Date beginDatum, Date eindDatum) {
        Query q = em.createNamedQuery("Score.getAvgClassGroupedByCourse");
        q.setParameter("vak", vak);
        q.setParameter("klas", klas);
        q.setParameter("beginDatum", beginDatum);
        q.setParameter("eindDatum", eindDatum);
        return (List<Object[]>)q.getResultList();
    }
    
    private int[] getAverageAndMedianKlasVakScore(String vak, String klas, Date beginDatum, Date eindDatum){
        int[] result = new int[2];
        Float sum = 0f;
        List<Object[]> queryResults;
        queryResults = getAvgClassGroupedByCourse(vak, klas, beginDatum, eindDatum);
        for (Object[] queryResult : queryResults) {
            sum += (int)(long)queryResult[0];
        }
        // first part of the result, namely the average
        result[0] = Math.round((sum / queryResults.size()));
        if(queryResults.size()%2 != 0){
            result[1] = (int)(long)queryResults.get(queryResults.size()/2)[0];
        }
        else{
            //Takes the 2 middle elements from the result list and takes the average of them to get the median, which is our second part of the result.
            result[1] = Math.round(((int)(long)queryResults.get(queryResults.size()/2)[0]
                    + (int)(long)queryResults.get((queryResults.size()/2) - 1)[0]) /2f);
        }
        return result;
    }
}
