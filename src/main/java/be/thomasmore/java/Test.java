/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author StijnSoete
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "Test.getByAllParams", query = "SELECT t FROM Test t WHERE t.naam = :naam AND t.vakNaam = :vakNaam AND t.maxScore = :maxScore AND t.datum = :datum"),
        @NamedQuery(name = "Test.getAll", query = "SELECT t FROM Test t")
})
@XmlRootElement
public class Test implements Serializable {
    private String naam,vakNaam;
    private int maxScore;
    @Temporal(TemporalType.DATE)
    private Date datum;

    public Test(String naam, String vakNaam, int maxScore, Date datum, Long id) {
        this.naam = naam;
        this.vakNaam = vakNaam;
        this.maxScore = maxScore;
        this.datum = datum;
        this.id = id;
    }

    public Test() {
    }

    public int getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(int maxScore) {
        this.maxScore = maxScore;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }
    
    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getVakNaam() {
        return vakNaam;
    }

    public void setVakNaam(String vakNaam) {
        this.vakNaam = vakNaam;
    }
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Test)) {
            return false;
        }
        Test other = (Test) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return "test " + naam + " voor " + vakNaam + " op " + maxScore + " punten (" + dateFormat.format(datum) + ")";
    }
    
}
