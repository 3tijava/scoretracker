package be.thomasmore.java;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author StijnSoete & deWitPeter-Paul
 */
public class ExcelReader{
    public List<Score> readTest(InputStream xslxFile) {
        List<Score> scores = new ArrayList<>();
        try {
            XSSFWorkbook workbook = new XSSFWorkbook(xslxFile);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Test test = new Test();
            for (Row row : sheet) {
                switch (row.getRowNum()) {
                    case 0:
                        test.setVakNaam(row.getCell(1).getStringCellValue());
                        break;
                    case 1:
                        test.setNaam(row.getCell(1).getStringCellValue());
                        break;
                    case 2:
                            try {
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                String cellValue = sdf.format(row.getCell(1).getDateCellValue());
                                Date date = (Date) sdf.parse(cellValue);
                                test.setDatum(date);
                            } catch (Exception e) {
                                e.printStackTrace();
                                test = null;
                            }
                        break;
                    case 3:
                        row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
                        try{
                        test.setMaxScore(Integer.parseInt(row.getCell(1).getStringCellValue()));
                        } catch(NumberFormatException e){
                            e.printStackTrace();
                            test = null;
                        }
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    default:
                        Score score = new Score();
                        Student student = new Student();
                        for (Cell cell : row) {
                            cell.setCellType(Cell.CELL_TYPE_STRING);
                            if (!cell.getStringCellValue().isEmpty()) {
                                switch (cell.getColumnIndex()) {
                                    case 0:
                                        student.setStudentNummer(cell.getStringCellValue());
                                        break;
                                    case 1:
                                        student.setFirstName(cell.getStringCellValue());
                                        break;
                                    case 2:
                                        student.setLastName(cell.getStringCellValue());
                                        break;
                                    case 3:
                                        try{
                                            score.setScore(Integer.parseInt(cell.getStringCellValue()));
                                        } catch(NumberFormatException e){
                                            e.printStackTrace();
                                            test = null;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        score.setStudent(student);
                        score.setTest(test);
                        scores.add(score);
                        break;
                }
                if(test == null){
                    scores = null;
                    break;
                }
            }
            xslxFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return scores;
    }

    public List<Student> readKlas(InputStream xslxFile) {
        List<Student> students = new ArrayList<Student>();
        try {
            XSSFWorkbook workbook = new XSSFWorkbook(xslxFile);
            XSSFSheet sheet = workbook.getSheetAt(0);
            String klas = sheet.getRow(0).getCell(1).getStringCellValue();
            for (Row row : sheet) {
                if (row.getRowNum() > 2) {
                    //lijst studenten invullen vanaf rij 3
                    Student student = new Student();
                    for (Cell cell : row) {
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (!cell.getStringCellValue().isEmpty()) {
                            switch (cell.getColumnIndex()) {
                                case 0:
                                    student.setStudentNummer(cell.getStringCellValue());
                                    break;
                                case 1:
                                    student.setFirstName(cell.getStringCellValue());
                                    break;
                                case 2:
                                    student.setLastName(cell.getStringCellValue());
                                    break;
                                case 3:
                                    student.setEmail(cell.getStringCellValue());
                                default:
                                    break;
                            }
                        }
                    }
                    if(student.getStudentNummer() != null){
                        student.setKlas(klas);
                        students.add(student);
                    }
                }
            }
            xslxFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return students;
    }
}
