package be.thomasmore.java;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Element;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

/**
 *
 * @author StijnSoete & deWitPeter-Paul
 */
@Stateless
@Path("testreport")
public class PDFService {

    @PersistenceContext(unitName = "MavenPU")
    private EntityManager em;

//    @GET
//    @Path("{testId}")
//    public Collection<Score> findStudentsByTestId(@PathParam("testId") Long testId) {
//        Query q = em.createNamedQuery("Score.getScoresByKlasAndTest");
//        q.setParameter("testId", testId);
//        return q.getResultList();
//    }
    public Test findTest(Long testId) {
        return em.find(Test.class, testId);
    }

    public void exportPdf(File temp) throws FileNotFoundException, IOException {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();

        response.setHeader("Content-Disposition", "attachment; filename=testRapport.pdf");
        response.setContentLength((int) temp.length());

        ServletOutputStream out;

        FileInputStream input = new FileInputStream(temp);
        byte[] buffer = new byte[1024];
        out = response.getOutputStream();
        int i = 0;
        while ((i = input.read(buffer)) != -1) {
            out.write(buffer);
            out.flush();
        }

        fc.responseComplete();
    }
    @POST
    @Path("getreports")
    @Produces(MediaType.TEXT_HTML)
    public String showReports() {
        String result = "<h1>Rapporten</h1>";
        
        result += dynamischeInhoud();
        return result;
    }
    
    public List<Test> getAllTesten() {
        Query q = em.createNamedQuery("Test.getAll");
        return q.getResultList();
    }
    
    public List<Student> getAllStudenten() {
        Query q = em.createNamedQuery("Student.getAllStudenten");
        return q.getResultList();
    }
    
    public String dynamischeInhoud(){
        String result = "";
        List<Test> testen = getAllTesten();
        List<Student> studenten = getAllStudenten();
        result += "<h2>Test rapporten</h2>";
        for(Test test: testen){
            result += "<p>rapport voor " + test.toString() + "</p>" +
"        <form action=\"../../../java/testreport/getpdf/" + test.getId() + "\">\n" +
"            <input type=\"submit\" value=\"Test rapport\"/>\n" +
"        </form><hr>";
        }
        result += "<h2>Student Rapporten</h2>";
        for(Student student: studenten){
            result += "<p>rapport voor " + student.toString() + " (eerste semester 2016-2017)</p>" +
            "<form action=\"../../../java/studentreport/getpdf/" + student.getStudentNummer() + "/01-09-2016/01-01-2017\">\n" +
    "            <input type=\"submit\" value=\"studenten rapport\"/>\n" +
    "        </form>";
            result += "<p>rapport voor " + student.toString() + " (tweede semester 2016-2017)</p>" +
            "<form action=\"../../../java/studentreport/getpdf/" + student.getStudentNummer() + "/01-01-2017/01-07-2017\">\n" +
    "            <input type=\"submit\" value=\"studenten rapport\"/>\n" +
    "        </form><hr>";
        }
        return result;
    }
    
    @GET
    @Path("getpdf/{testId}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadTestPdf(@PathParam("testId") Long testId) {
        File temp = createPDFTest(testId);
        ResponseBuilder response = Response.ok((Object) temp);
        response.header("Content-Disposition", "attachment; filename=testRapport.pdf");
        return response.build();
    }

    public List<Score> getScoresByTest(Long testId) {
        Query q = em.createNamedQuery("Score.getScoresByTest");
        q.setParameter("testId", testId);
        return q.getResultList();
    }
    
    public float getGemiddelde(List<Score> scores){
        int sum = 0;
        for(Score score: scores)
            sum += score.getScore();
        return (float)sum / (float)scores.size();
    }

    public float getMediaan(List<Score> scores){
        if(scores.size()%2 != 0){
            return Math.round((float) scores.get(scores.size()/2).getScore());
        }
        else{
            //Takes the 2 middle elements from the result list and takes the average of them to get the median, which is our second part of the result.
            return ((float)scores.get(scores.size()/2).getScore()
                    + (float)scores.get((scores.size()/2) - 1).getScore()) / 2f;
        }
    }
    
    public File createPDFTest(Long testId) {
        try {
            Test test = findTest(testId);
            List<Score> scores = getScoresByTest(test.getId());
            Document document = new Document();
            // Tijdelijk bestand aanmaken (PDF)
            File temp = File.createTempFile("resultaat" + test.getNaam(), ".pdf");
            //PDF openen en bewerken
            PdfWriter.getInstance(document, new FileOutputStream(temp.getAbsolutePath()));
            document.open();
            // MetaData toevoegen
            document.addTitle("Resulaat_ " + test.getNaam());
            document.addAuthor("ScoreTracker");
            document.addCreator("ScoreTracker");
            Font titleFont = new Font(FontFactory.getFont(FontFactory.TIMES, 18f));
            titleFont.setColor(33, 84, 172);

            Font subtitleFont = new Font(FontFactory.getFont(FontFactory.TIMES, 16f));
            subtitleFont.setColor(29, 100, 150);

            Font boldFont = new Font(FontFactory.getFont(FontFactory.TIMES_BOLD, 14f));
            boldFont.setColor(0, 0, 0);
            
            // Titel toevoegen
            Paragraph preface = new Paragraph();
            Paragraph preface1 = new Paragraph();
            Paragraph gemiddelde = new Paragraph();
            Paragraph mediaan = new Paragraph();
            preface.add(new Paragraph("Test " + test.getNaam() + " Vak: " + test.getVakNaam(), titleFont));
            preface1.add(new Paragraph("Maximum score:" + test.getMaxScore(), titleFont));
            gemiddelde.add(new Paragraph("Gemiddelde score van de klas: " + getGemiddelde(scores), boldFont));
            mediaan.add(new Paragraph("Mediaan van de klas: " + getMediaan(scores), boldFont));
            document.add(preface);
            document.add(preface1);
            document.add(gemiddelde);
            document.add(mediaan);
            // Tabel toevoegen met 2 kolommen
            PdfPTable table = new PdfPTable(2);
            PdfPCell c1 = new PdfPCell(new Phrase("Student", subtitleFont));
            c1.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(c1);
            PdfPCell c2 = new PdfPCell(new Phrase("Resultaat", subtitleFont));
            c2.setHorizontalAlignment(Element.ALIGN_LEFT);
            table.addCell(c2);
            table.setHeaderRows(1);
            //tabel opvullen met scores uit "List<Score> scores"
            for (Score score : scores) {
                table.addCell(score.getStudent().getFirstName() + " " + score.getStudent().getLastName());
                table.addCell(score.getScore() + "");
            }

            // breedte van de kolommen
            float[] columnWidths = new float[]{30f, 30f};
            table.setWidths(columnWidths);

            document.add(table);

            document.close();
            return temp;
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
            return null;
        }
    }
}
