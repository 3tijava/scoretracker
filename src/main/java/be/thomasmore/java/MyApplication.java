
package be.thomasmore.java;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/java")
public class MyApplication extends ResourceConfig {

    public MyApplication() {
        super(MultiPartFeature.class);
        packages("be.thomasmore.java"); // add all packages containing MultiPartFeature
    }
}
