
package be.thomasmore.java;

import java.io.InputStream;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author StijnSoete
 */

@Stateless
@Path("klas")
public class KlasService {

    private final ExcelReader excelReader = new ExcelReader();
    @PersistenceContext(unitName = "MavenPU")
    private EntityManager em;

    public List<Student> readKlasFromExcel(InputStream xslxFile) {
        List<Student> students = excelReader.readKlas(xslxFile);
        return students;
    }

    @POST
    @Path("uploadKlas")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    public String readKlasFromExcelAndStore(@FormDataParam("file") InputStream xslxFile,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {
        List<Student> students = storeKlas(readKlasFromExcel(xslxFile));
        if(!students.isEmpty()){
            String output = "<h1>Ingelezen leerlingen in klas " + students.get(0).getKlas() + "</h1>";
            for(Student student: students){
                output += "<p>" + student.getStudentNummer() + ": " + student.getFirstName() + " " + student.getLastName() + ", met emailadres '" + student.getEmail() + "'</p>";
            }
            return output;
        }
        else
            return "<h1>Geen nieuwe studenten gevonden</h1><p>Dit kan veroorzaakt worden door een file die niet voldoet aan de gegeven template. Gelieve dit na te kijken.</p>";
    }

    public List<Student> storeKlas(List<Student> students) {
        for (Student student : students) {
            //If the student already exists this part makes sure it doesnt duplicate it and update the values of the old one.
            try{
                Query q = em.createNamedQuery("Student.getStudentByRNummer");
		q.setParameter("rNummer", student.getStudentNummer());
                Student oldStudent = (Student) q.getSingleResult();
                // If any of the following data changed it will be updated in the database.
                oldStudent.setEmail(student.getEmail());
                oldStudent.setFirstName(student.getFirstName());
                oldStudent.setLastName(student.getLastName());
                oldStudent.setKlas(student.getKlas());
                
                em.persist(oldStudent);
            }
            catch(NoResultException e){
                //This is fine, it just means this particular student doesn't exist in the database yet.
                em.persist(student);
            }
        }
        return students;
    }

    @GET
    @Path("{studentId}")
    public Student findStudentById(@PathParam("studentId") Long studentId) {
        return em.find(Student.class, studentId);
    }

    @GET
    @Path("all")
    public List<Student> findAllStudents() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Student.class));
        return em.createQuery(cq).getResultList();
    }

}
