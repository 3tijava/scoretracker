<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Scoretracker</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    </head>

    <body>
        <header>
            <nav class="container-fluid header">
                <div class="row">
                    <div class="col-lg-8 col-md-6 col-sm-6 col-xs-6" id="hoofding">
                        <h1>Scoretracker</h1>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
                        <ul class="navigatie-menu">
                            <li role="presentation">
                                <a href="###">GEBRUIKER</a>
                            </li>
                            <span></span>
                            <li>
                                <a href="###"><i class="fa fa-sign-out"></i>  Uitloggen</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav nav-pills navigatie-nav">  
                        <li role="presentation" class="active"><a href="#"><i class="fa fa-home"></i>Home</a></li>
                        <li role="presentation"><a href="uploaden.jsp">Uploaden</a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="container">
            <h1>Klik op volgende button om rapporten in te kijken</h1>
            <form action="java/testreport/getreports/"  method="POST">
                <input type="submit" value="rapporten"/>
            </form>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>