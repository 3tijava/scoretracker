/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author StijnSoete
 */
public class AppTest {

    private ExcelReader excelReader = new ExcelReader();
    private KlasService klasService = new KlasService();
    private TestService testService = new TestService();
    private InputStream testFileTest;
    private InputStream testFileKlas;

    @org.junit.Before
    public void setUp() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        testFileKlas = classLoader.getResourceAsStream("3Ti_Klas.xlsx");
        testFileTest = classLoader.getResourceAsStream("3Ti_Toets1.xlsx");
    }

    @Test
    public void testReadKlas() throws Exception {
        List<Student> readList = excelReader.readKlas(testFileKlas);
        List<Student> hardCodedList = new ArrayList<Student>();
        hardCodedList.add(new Student("stijn", "soete", "r0459749@student.thomasmore.be", "3Ti", "r0459749"));
        hardCodedList.add(new Student("Peter-Paul","de Wit","r0591954@student.thomasmore.be","3Ti","r0591954"));
        Assert.assertEquals(readList, hardCodedList);
    }

    @Test
    public void testReadKlasFail() throws Exception {
        List<Student> readList = excelReader.readKlas(testFileKlas);
        List<Student> hardCodedList = new ArrayList<Student>();
        hardCodedList.add(new Student("stijn", "soete", "r0459749@student.thomasmore.be", "3Ti", "r0459749"));
        hardCodedList.add(new Student("jef", "mast", "jef123@hotmail.com", "lul", "r045788"));
        Assert.assertNotEquals(readList, hardCodedList);
    }

    @Test
    public void testReadTest() throws Exception {
        List<Score> readList = excelReader.readTest(testFileTest);
        List<Score> hardCodedList = new ArrayList<Score>();
        hardCodedList.add(new Score(17));
        Assert.assertEquals(readList, hardCodedList);
    }

    @Test
    public void testReadTestFail() throws Exception {
        List<Score> readList = excelReader.readTest(testFileTest);
        List<Score> hardCodedList = new ArrayList<Score>();
        hardCodedList.add(new Score(15));
        hardCodedList.add(new Score(18));
        Assert.assertNotEquals(readList, hardCodedList);

    }
    
    

}
